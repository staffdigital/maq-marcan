$('#openMenu').click(function(event) {
	$('.menu_link').toggleClass('showMenu');
});

// ANCLA HEADER
$(document).ready(function($) {
	function location_url(){
		var w_url = window.location.pathname
		$('a[data-url="'+w_url+'"]').trigger('click');
	}
	location_url();
	});

	function trackingLink() {
		var href = window.location.href; var url = href.split('?z=');
		history.pushState(null, null, url[0]);
	}

	 $(function () {
	 	function wLinkerNav(url_obj,home=false){
			if(home){
				window.history.pushState(200, "Marcan", '/');
			}else{
				window.history.pushState(200, url_obj.attr('data-title'), url_obj.attr('data-url'));
			}
		}

		var b1_block = $('#banner');
		var b2_block = $('#proyecto');
		var b3_block = $('#galeria');
		var b4_block = $('#planos');
		var b5_block = $('#ubicacion');
		var b6_block = $('#video');

		var menu_a1 = $(".menu_a1");
		var menu_a2 = $(".menu_a2");
		var menu_a3 = $(".menu_a3");
		var menu_a4 = $(".menu_a4");
		var menu_a5 = $(".menu_a5");

		b2_block.waypoint(function(direction) {
			if (direction === 'down') {
				menu_a1.addClass('active');
				wLinkerNav(menu_a1);
			}else{
				menu_a1.removeClass('active');
				wLinkerNav(menu_a1,true);
			}
		}, {
			offset:'60%'
		});

		b3_block.waypoint(function(direction) {
			if (direction === 'down') {
				menu_a2.addClass('active');
				menu_a1.removeClass('active');
				wLinkerNav(menu_a2);

			}else{
				menu_a1.addClass('active');
				menu_a2.removeClass('active');
				wLinkerNav(menu_a1);
			}
		}, {
			offset:'60%'
		});

		b4_block.waypoint(function(direction) {
			if (direction === 'down') {
				menu_a3.addClass('active');
				menu_a2.removeClass('active');
				wLinkerNav(menu_a3);

			}else{
				menu_a2.addClass('active');
				menu_a3.removeClass('active');
				wLinkerNav(menu_a2);
			}
		}, {
			offset:'60%'
		});

		b5_block.waypoint(function(direction) {
			if (direction === 'down') {
				menu_a4.addClass('active');
				menu_a3.removeClass('active');
				wLinkerNav(menu_a4);

			}else{
				menu_a3.addClass('active');
				menu_a4.removeClass('active');
				wLinkerNav(menu_a3);
			}
		}, {
			offset:'60%'
		});

		b6_block.waypoint(function(direction) {
			if (direction === 'down') {
				menu_a5.addClass('active');
				menu_a4.removeClass('active');
				wLinkerNav(menu_a5);

			}else{
				menu_a4.addClass('active');
				menu_a5.removeClass('active');
				wLinkerNav(menu_a4);			}
		}, {
			offset:'60%'
		});


		// Ancla scroll - AGREGAR CLASE DEL ENLACE
		$('.miclase').click(function() {
			if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')&& location.hostname == this.hostname) {
				var $target = $(this.hash);
				$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
				if ($target.length) {
				var targetOffset = $target.offset().top - 60;
				$('html,body').animate({scrollTop: targetOffset}, 1000);
				return false;
				}
			}
		});

	});